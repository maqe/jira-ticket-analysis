import urllib.parse
import urllib.request
import base64
import json
from datetime import datetime
from datetime import timezone

from Project import Project


JIRA_API_ENDPOINT = "https://maqecom.atlassian.net/rest/api/2/"

class Jira:
	emailToken = None

	def __init__(self, emailToken):
		self.emailToken = base64.b64encode(emailToken.encode('ascii')).decode('ascii')

	def getHeaders(self):
		headers = {
			"Authorization": "Basic " + self.emailToken,
			"Content-Type": "application/json"
		}
		return headers
	
	def doRequest(self, url):
		request = urllib.request.Request(JIRA_API_ENDPOINT + url, headers=self.getHeaders(), method='GET')
		try:
			request = urllib.request.urlopen(request)
		except urllib.error.URLError as e:
			print(e)
			return None

		response = request.read().decode('utf-8')
		return json.loads(response)

	def getProjects(self):

		data = self.doRequest('project')

		projects = []
		for rawProject in data:
			projects.append(Project(self, rawProject))

		return projects
