Uses JIRA REST API to find all tickets in a particular project and then counts them per sprint, per person.
Originally created for OKR to measure how many tickets are dragged back from testing.