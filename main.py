# Coded by Ignas
# Uses JIRA REST API to find all tickets in a particular project and then counts them per sprint, per person.
# Originally created for OKR to measure how many tickets are dragged back from testing.

import sys
from time import sleep
from datetime import datetime
from datetime import timezone

from Jira import Jira

statusBeforeTest = ['backlog', 'in progress']

statusTest = ['ready for test', 'testing']
statusFinal = ['done', 'closed']

statusNotDevelopment = ['ready for test', 'testing', 'done', 'closed']

"""
	Looking at ticket history, attempt to detect lightning pattern:
	statusBeforeTest -> statusTest -> statusBeforeTest -> statusFinal
	Since history is in reverse order, so is the pattern.
"""
#Proper full pattern
#lightningPattern = [statusFinal, statusBeforeTest, statusTest, statusBeforeTest]

# Simplified pattern
lightningPattern = [statusFinal, statusBeforeTest, statusNotDevelopment, statusBeforeTest]


if (len(sys.argv) >= 2):
	jiraUser = sys.argv[1]

else:
	print("Go to https://id.atlassian.com/manage/api-tokens and create a token.\nPlease enter youremail@example.com:yourtoken")
	jiraUser = input()

jira = Jira(jiraUser)

projects = jira.getProjects()

if (not projects):
	print("Your account is not associated with any projects")
	exit()

print("Following projects were found:")
sleep(1)
orderNo = 1
for project in projects:
	print("#{} [{}] {}".format(orderNo, project.getKey(), project.getName()))
	orderNo += 1

print("Pick specific project. Enter project #:")
projectNo = int(input())

activeProject = projects[projectNo-1]
print("\nYou've picked [{}] {}".format(activeProject.getKey(), activeProject.getName()))

print("\nPick one: #1 - normal run #2 discovery run")

if (int(input()) == 2):
	activeProject.discovery()
	exit()

"""
	Filter function that controls whether the issue is added to the list.
	return True - continue fetching issues
	return False - stop fetching issues and return current issue list
"""
def filterFn(issues, issue):
	# Only include issue if it has sprint set
	if (not issue.sprint_name):
		return True

	# Only include issue if it has assignee set
	if (not issue.assignee_name):
		return True

	# Only include issues that are currently done
	if (issue.status_name not in statusFinal):
		return True

	createdTime = issue.created
	# Only include issues in range of 2019 1 to 2019 5
	#if (createdTime < datetime(2019, 1, 1, tzinfo=timezone.utc) or createdTime >= datetime(2019, 6, 1, tzinfo=timezone.utc)):
	#	return True

	if (projectNo is 5): # Insee
		if (not (
			issue.assignee_name.startswith('Chalermpong')
			or issue.assignee_name.startswith('Kraiwoot')
			or issue.assignee_name.startswith('Sikarin')
			or issue.assignee_name.startswith('Sirawit')
			)
		):
			return True
	elif (activeProject.getKey() == 'FM'): # Capcito
		if (not (
			issue.assignee_name.startswith('Ignas Poklad')
			or issue.assignee_name.startswith('Raf - Ashraf al-Kibsi')
			or issue.assignee_name.startswith('Chanchai Lee')
			)
		):
			return True
	elif (projectNo is 24): # Chanin
		if (not (
			issue.assignee_name.startswith('Jesada')
			or issue.assignee_name.startswith('Jakraphan Tewarugpitug')
			or issue.assignee_name.startswith('Nipapan Aom')
			)
		):
			return True

	if (issue.type_name != 'bug'):
		return True

	issues.append(issue)
	#return False # stop as soon as first ticket matching all the criteria is found
	return True

issues = activeProject.getIssues(filterFn)

results = {}

for issue in issues:
	createdTime = issue.created
	
	# 2019.01, 2019.02, 2019.03
	if (createdTime < datetime(2019, 4, 1, tzinfo=timezone.utc)):
		isPrevious = True
	else:
		isPrevious = False

	lightningIndicator = issue.HistoryMatchesPattern('status', lightningPattern)
	
	print("{},{},{},{},{},{},{},{},{}".format(
		createdTime.strftime('%Y-%m-%d'),
		isPrevious,
		issue.type_name,
		projectNo,
		issue.key,
		issue.assignee_name,
		lightningIndicator,
		issue.status_category_id,
		issue.summary,
		))

	"""
		Key controls the aggregation. In SQL terms, GROUP BY
		The order of field in the key controls the sorting, ORDER BY
	"""
	"""
	sprint = issue.sprint_name
	key = sprint + ' - '+ issue.assignee_name
	if (key not in results):
		results[key] = {"total":0, "indicator":0}

	results[key]["total"] += 1
	if (lightningIndicator):
		results[key]["indicator"] += 1
	"""

# To see which fields are available in the ticket https://jsonformatter.curiousconcept.com/
#print(issues[0].raw)
# exit()