from Issue import Issue
from Issue import ISSUE_VALUE
class Project:

	raw = None
	def __init__(self, jira, rawProject):
		self.raw = rawProject
		self.jira = jira


	def getKey(self):
		return self.raw['key']


	def getName(self):
		return self.raw['name']


	"""
		This function can be used to get the issues(tickets) for a particular project.
		filterFunction is called with the entire list of the isseus and the current issue
			is responsible for appending issues to the list
			return True - continue fetching issues
			return False - stop fetching issues and return current issue list
	"""
	def getIssues(self, filterFunction):

		issues = []
		issuePos = 0
		issueCount = 100

		while issuePos < issueCount:
			data = self.jira.doRequest("search?jql=project={}&expand=changelog&maxResults=100&startAt={}".format(self.getKey(), issuePos))

			issuePos += int(data['maxResults'])
			issueCount = int(data['total'])

			for rawIssue in data['issues']:
				issue = Issue(rawIssue)
				if not filterFunction(issues, issue):
					return issues

		return issues

	def discovery(self):
		
		fieldNames = []
		fieldValues = {}
		
		for key in ISSUE_VALUE.keys():
			if (('disvoerySkip' in ISSUE_VALUE[key]) or ('continuous' in ISSUE_VALUE[key])):
				continue

			fieldNames.append(key)
			fieldValues[key] = []

		issues = []
		issuePos = 0
		issueCount = 100

		while issuePos < issueCount:
			data = self.jira.doRequest("search?jql=project={}&expand=changelog&maxResults=100&startAt={}".format(self.getKey(), issuePos))

			issuePos += int(data['maxResults'])
			issueCount = int(data['total'])

			for rawIssue in data['issues']:
				issue = Issue(rawIssue)
				for key in fieldNames:
					value = getattr(issue, key)
					if (value not in fieldValues[key]):
						fieldValues[key].append(value)

		for key in fieldNames:
			print(key)
			for value in fieldValues[key]:
				print('     {}'.format(value))
			print('')