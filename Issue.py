from datetime import datetime

ISSUE_VALUE = {
	'sprint_name': {
		'fn': '_getSprintName'
	},
	'assignee_name': {
		'fn': '_getAssigneeName'
	},
	'type_name': {
		'fn': '_getTypeName'
	},
	'status_category_id': {
		'fn': '_getStatusCategoryId'
	},
	'status_name': {
		'fn': '_getStatusName'
	},
	'key': {
		'fn': '_getKey'
	},
	'summary': {
		'fn': '_getSummary',
		'disvoerySkip': True
	},
	'created': {
		'fn': '_getCreated',
		'continuous': True
	}
}

class Issue:
	def __init__(self, raw):
		self.raw = raw


	# Used for compute issue attributes on the fly and then chache them
	def __getattr__(self, name):
		if (name in ISSUE_VALUE):
			method = getattr(self, ISSUE_VALUE[name]['fn'])
			val = method()
			setattr(self, name, val)
			return val

		raise AttributeError


	def _getSprintName(self):

		for key, value in self.raw['fields'].items():
			if (not value or not isinstance(value, list)):
				continue

			value = value[0]

			# The field name will start with customfield_
			if (not key.startswith('customfield_')):
				continue
			# Detect sprint field beginning
			if (not value.startswith('com.atlassian.greenhopper.service.sprint')):
				continue
			# Find name beginning
			nameStartPos = value.find('name=')
			if (nameStartPos == -1):
				continue
			# Find name ending
			nameEndPos = value.find(',',nameStartPos)

			return value[nameStartPos + 5 : nameEndPos]

		return None


	def _getAssigneeName(self):
		if (not self.raw['fields']['assignee']):
			return None
		return self.raw['fields']['assignee']['displayName']


	def _getTypeName(self):
		return self.raw['fields']['issuetype']['name'].lower()


	def _getStatusName(self):
		return self.raw['fields']['status']['name'].lower()


	def _getStatusCategoryId(self):
		return self.raw['fields']['status']['statusCategory']['key']


	def _getCreated(self):
		return self._parseDateTime(self.raw['fields']['created'])


	def _getKey(self):
		return self.raw['key']


	def _getSummary(self):
		return self.raw['fields']['summary']


	def _parseDateTime(self, str):
		# JIRA stores dates in this format: 2019-05-17T20:50:37.103+0700
		return datetime.strptime(str, '%Y-%m-%dT%H:%M:%S.%f%z')

	def HistoryMatchesPattern(self, fieldName, pattern):
		patternPos = 0
		patternLen = len(pattern)
		
		for historyEntry in self.raw['changelog']['histories']:
			for EntryItem in historyEntry['items']:
				if (EntryItem['field'] == fieldName):
					fieldValue = EntryItem['toString'].lower()
					if (fieldValue in pattern[patternPos]):
						patternPos += 1
						if (patternPos >= patternLen):
							return True
		return False